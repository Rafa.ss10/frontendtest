const express = require ("express")
const app = express()
const fetch = require("node-fetch")
const cors = require("cors")

app.get("/:postcode", async (req, res) => {
  const postcode = req.params.postcode;
  const response = await fetch(`https://uk.api.just-eat.io/restaurants/bypostcode/${postcode}`);
  const json = await response.json();

  const restaurantsString = json.Restaurants.map((restaurant) => {
    return `${restaurant.Id},${restaurant.Name},${restaurant.Address.Postcode},${restaurant.Rating.Average},${restaurant.CuisineTypes[0].Name}`;
  }).join(',');

  console.log(restaurantsString);
  res.send(restaurantsString);
});

app.listen(3000, () => {
   console.log("Listening on port  3000")
})