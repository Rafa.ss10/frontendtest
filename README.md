# FrontEndtest
#My angular app for smartex

git clone https://gitlab.com/Rafa.ss10/frontendtest.git

I will use this as a place to answer some technical questions:

Firstly, I have recently started my frontend preparation studying javascript after having learned other languages in the past. My frontend level wasn´t the best and I struggled a little bit because I was learning and doing the test at the same time. The app is not finished and has a problem in the connection between the API and the way it displays on the screen when the postcodes are asked. Because of CORS I did a proxy server to work as a middleman between the API and my localhost. The way my Angular App receives the info from the proxy and then manages it is where the problem resides. With more time I would correct it. 


The way I presented things, from the HTML to the CSS isn´t the best either. I focused on making it work and left the estetic aside (another thing I would focus on with more time).

I have never track down a performance issue in production but have some lights on how to do it. It is normaly recommended to identify the issue and gather as much info as possible about it. Then it is about to analyse the data gathered and to reproduce the issue in a test environment. Finally I would talk to the team involved and implement the solution.

This was a learning experience that made me work hard to be able to learn and apply everything at the same time. Thank you for the oportunity regardless of not being able to fully complete the challenge wihin the 72h.




