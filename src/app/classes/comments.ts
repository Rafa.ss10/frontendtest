

export class Comments
{
    cuisine: string;
    restaurantName: string;
    brandName: string;
    ratingsOutOfFive: boolean;
}