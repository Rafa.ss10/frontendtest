import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  getRestaurantsByPostcode(postcode: string): any {
    throw new Error('Method not implemented.');
  }
  private apiEndpointUrl = 'https://api.example.com/comments';

  constructor(private http: HttpClient) {}

  getcomments(postcode: string) {
    const url = `${this.apiEndpointUrl}?postcode=${postcode}`;
    return this.http.get<Comment[]>(url);
  }
}
