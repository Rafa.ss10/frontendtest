import { Component, ViewChild } from '@angular/core';
import { ApiService } from './services/api.service';
import { MatTable } from '@angular/material/table';
import { map } from 'rxjs';

export interface Comment {
  cuisine: string;
  restaurantName: string;
  brandName: string;
  ratingsOutOfFive: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild(MatTable) table!: MatTable<any>;
  displayedColumns: string[] = [ 'restaurantName', 'cuisine', 'ratingsOutOfFive'];
  dataSource: Comment[]=[];
  restaurants: any;
  postcode: string;

  constructor(private apiService: ApiService) {}

  onSubmit() {
    const input = document.getElementById('postcode') as HTMLInputElement;
    this.postcode = input.value;
    console.log(this.postcode);
    this.apiService.getRestaurantsByPostcode(this.postcode)
      .pipe(
        map((restaurants: any[]) => {
          return restaurants.map((restaurant: any) => {
            return {
              cuisine: restaurant.CuisineTypes[0].Name,
              restaurantName: restaurant.Name,
              brandName: restaurant.BrandName,
              ratingsOutOfFive: restaurant.Rating.Average.toString()
            };
          });
        })
      )
      .subscribe((dataSource: any[]) => {
        this.dataSource = dataSource;
        console.log(this.dataSource)
        this.table.renderRows();
      });
  }


}
